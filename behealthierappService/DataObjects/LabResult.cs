﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace behealthierappService.DataObjects
{
    public class LabResult:EntityData
    {

        [Required]
        public string AccountId { get; set; }


        [Required]
        public string AnalyteId { get; set; }

        public DateTime LabResultDate { get; set; }
    }
}