﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace behealthierappService.DataObjects
{
    public class AlertRecord:EntityData
    {

        [Required]
        public string AccountId { get; set; }

        public bool PushNotificationStatus { get; set; }

        public DateTime AlertDate { get; set; }

        [Required]
        public string MedikitAlertId { get; set; }
  

        public int SnoozeCount { get; set; }

        public bool Taken { get; set; }

        public string ScheduledTime { get; set; }

        public string ActualTimeRespondedTo { get; set; }
    }
}