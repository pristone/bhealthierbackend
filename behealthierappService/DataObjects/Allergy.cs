﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace behealthierappService.DataObjects
{
    public class Allergy:EntityData
    {

        [Required]
        public string AccountId { get; set; }

        public enum AllergyTypes { FOOD,DRUG,OTHER};
        public AllergyTypes AllergyType { get; set; }
        public string AllergyDescription { get; set; }
        public string AllergyTitle { get; set; }
    }
}