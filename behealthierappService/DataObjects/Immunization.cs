﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace behealthierappService.DataObjects
{
    public class Immunization:EntityData
    {
        [Required]
        public string AccountId { get; set; }

        public DateTime ImmunizationDate { get; set; }
        public string ImmunizationDescription { get; set; }
        public string ImmunizationTitle { get; set; }
        [Required]
        public string ProviderProfileID { get; set; }


    }
}