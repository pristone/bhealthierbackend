﻿using Microsoft.Azure.Mobile.Server;

namespace behealthierappService.DataObjects
{
    public class Account:EntityData
    {
        public string Username { get; set; }
        public byte[] Salt { get; set; }
        public byte[] SaltedAndHashedPassword { get; set; }
    }
}