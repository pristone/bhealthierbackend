﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.Azure.Mobile.Server;

namespace behealthierappService.DataObjects
{
    public class MedikitAlert: EntityData
    {

        public enum UOMS
        {
            Pills,
            Cc,
            Mg,
            Teaspoon,
            Tablespoon,
            Drops
        };

        public enum Intervals { IntervalDaily, IntervalWeekly, IntervalEvery2Weeks,IntervalMonthly,IntervalYearly };

        public enum DuringTimes { DuringOneDay, DuringTwoDay, DuringThreeDay, DuringFourDay, DuringFiveDay, DuringXDays, DuringOneWeek, DuringTwoWeek, DuringThreeWeek, DuringOneMonth, DuringTwoMonth, DuringThreeMonth, DuringForever }

        [Required]
        public string AccountId { get; set; }

        public string AlertName { get; set; }

        public int Quantity { get; set; }

        public int Dosage { get; set; }

        public UOMS DosageUOM { get; set; }

        public DateTime StartDate { get; set; }

        public string Notes { get; set; }

        public string CustomizedReminderMessage { get; set; }

        public bool AlertSound { get; set; }

        public string RefillSettingsId { get; set; }

        public Intervals Interval { get; set; }

        public DuringTimes During { get; set; }

        public string Provider { get; set; }

        public int Total { get; set; }

        public int Left { get; set; }

        public string TimeSchedules { get; set; }

        public bool IsActive { get; set; }

        [Required]
        public string MedicineId { get; set; }

        public DateTime EndDate { get; set; }

        public DateTime CreatedDate { get; set; }

        public int DaysSupply { get; set; }
    }
}