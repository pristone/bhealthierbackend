﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace behealthierappService.DataObjects
{
    public class Analyte : EntityData
    {
        public enum AnalyteTypes{EXPECTED,ACTUAL }
        public AnalyteTypes AnalyteType { get; set; }
        public string AnalyteDesc { get; set; }
        public string AnalyteUOM { get; set; }

        [Required]
        public string AccountId { get; set; }
    }
}