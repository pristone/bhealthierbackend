﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace behealthierappService.DataObjects
{
    public class HealthInfo:EntityData
    {
        public float Weight { get; set; }

        public float Height { get; set; }

        public float BloodPressure { get; set; }

        [Required]
        public string AccountId { get; set; }
    }
}