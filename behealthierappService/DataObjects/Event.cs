﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace behealthierappService.DataObjects
{
    public class Event:EntityData
    {
        [Required]
        public string AccountId { get; set; }

        public string EventName { get; set; }

        public string EventDescription { get; set; }

        public DateTime EventDate { get; set; }

        public bool Exported { get; set; }

        public string ProviderProfileID { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime EndDate { get; set; }
    }
}