﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace behealthierappService.DataObjects
{
    public class NoteForDoctor:EntityData
    {
        [Required]
        public string providerProfileId { get; set; }

        [Required]
        public string AccountId { get; set; }

        public string Note { get; set; }

        public DateTime NoteDate { get; set; }

        public string NoteTitle { get; set; }
    }
}