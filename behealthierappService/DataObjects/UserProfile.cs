﻿using Microsoft.Azure.Mobile.Server;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System;

namespace behealthierappService.DataObjects
{
    public class UserProfile:EntityData
    {
        public enum GenderVals { Male,Female,Other}
        public enum EthnicityVals { WhiteAmerican, BlackOrAfricanAmerican, NativeAmerican,AsianAmerican,HispanicOrLatinoAmerican,Other }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public bool Authenticated { get; set; }
        public string Email { get; set; }
        public long Dob { get; set; }
        public EthnicityVals Ethnicity { get; set; }
        public bool AcceptedTermsOfService { get; set; }
        public string AuthenticationCode { get; set; }
        public GenderVals Gender { get; set; }
        public string PhoneNum { get; set; }

        [Required]
        public string AccountId { get; set; }
    }
}