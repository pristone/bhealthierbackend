﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace behealthierappService.DataObjects
{
    public class RefillSetting:EntityData
    {

        [Required]
        public string AccountId { get; set; }

        public enum RefillReminderType { WhenXDosage, OnSpecificDate };

        public string RefillDayAlert { get; set; }

        public bool Alert { get; set; }

        public string PharmacyName { get; set; }

        public RefillReminderType RefillType { get; set; }

        public int RefillSchedule { get; set; }

        public long EstimatedRefillDate { get; set; }

        public bool IsActive { get; set; }

        public string Provider { get; set; }
    }
}