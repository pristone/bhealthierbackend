﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace behealthierappService.DataObjects
{
    public class Supply:EntityData
    {

        [Required]
        public string AccountId { get; set; }

        [Required]
        public string AdherenceId { get; set; }

        public int DaysSupply { get; set; }

        public DateTime EstimtedRefillDate { get; set; }

        public bool Export { get; set; }

    }
}