﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace behealthierappService.DataObjects
{
    public class MedDiary:EntityData
    {
        [Required]
        public string AccountId { get; set; }


        public string AllergyId { get; set; }

        public string ImmunizationId { get; set; }

        public string NoteForDoctorId { get; set; }
    }
}