﻿using System;

namespace behealthierappService.Models.RequestModels
{
    public class AuthenticationRequest
    {
        public String username { get; set; }
        public String password { get; set; }
    }
}