﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace behealthierappService.Models.RequestModels
{
    public class AuthorizationCodeRequest
    {
        public string authenticationCode { get; set; }
        public string userId { get; set; }
    }
}