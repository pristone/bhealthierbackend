﻿namespace behealthierappService.Models.RequestModels
{
    public class RegistrationRequest
    {
        public string username { get; set; }
        public string password { get; set; }
        public long dob { get; set; }
        public string email { get; set; }
        public DataObjects.UserProfile.GenderVals sex { get; set; }
        public DataObjects.UserProfile.EthnicityVals ethnicity { get; set; }
        public string phonenum { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public bool acceptedtermsofservice { get; set; }
    }
}