﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using Microsoft.Azure.Mobile.Server;
using Microsoft.Azure.Mobile.Server.Tables;
using behealthierappService.DataObjects;

namespace behealthierappService.Models
{
    public class behealthierappContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to alter your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx

        private const string connectionStringName = "Name=MS_TableConnectionString";

        public behealthierappContext() : base(connectionStringName)
        {
        } 

        public DbSet<TodoItem> TodoItems { get; set; }

        public DbSet<Account> Accounts { get; set; }

        public DbSet<UserProfile> UserProfiles { get; set; }

        public DbSet<RefillSetting> RefillSettings { get; set; }

        public DbSet<ProviderProfile> ProviderProfiles { get; set; }

        public DbSet<NoteForDoctor> NotesForDoctors { get; set; }

        public DbSet<Medicine> Medicines { get; set; }

        public DbSet<MedDiary> MedDiaries { get; set; }

        public DbSet<LabResult> LabResults { get; set; }

        public DbSet<HealthInfo> HealthInfos { get; set; }

        public DbSet<Analyte> Analytes { get; set; }

        public DbSet<Allergy> Allergies { get; set; }

        public DbSet<AlertRecord> AlertRecords { get; set; }

        public DbSet<Adherence> Adherences { get; set; }

        public DbSet<Event> Events { get; set; }

        public DbSet<Immunization> Immunizations { get; set; }

        public DbSet<MedikitAlert> MedikitAlerts { get; set; }

        public DbSet<Supply> Supplies { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Conventions.Add(
                new AttributeToColumnAnnotationConvention<TableColumnAttribute, string>(
                    "ServiceTableColumn", (property, attributes) => attributes.Single().ColumnType.ToString()));
        }



        
    }

}
