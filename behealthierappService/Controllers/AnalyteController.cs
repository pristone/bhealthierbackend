﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using behealthierappService.DataObjects;
using behealthierappService.Models;

namespace behealthierappService.Controllers
{
    public class AnalyteController : TableController<Analyte>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            behealthierappContext context = new behealthierappContext();
            DomainManager = new EntityDomainManager<Analyte>(context, Request);
        }

        // GET tables/Analyte
        public IQueryable<Analyte> GetAllAnalyte()
        {
            return Query(); 
        }

        // GET tables/Analyte/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<Analyte> GetAnalyte(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/Analyte/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<Analyte> PatchAnalyte(string id, Delta<Analyte> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/Analyte
        public async Task<IHttpActionResult> PostAnalyte(Analyte item)
        {
            Analyte current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/Analyte/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteAnalyte(string id)
        {
             return DeleteAsync(id);
        }
    }
}
