﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using behealthierappService.DataObjects;
using behealthierappService.Models;

namespace behealthierappService.Controllers
{
    public class AllergyController : TableController<Allergy>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            behealthierappContext context = new behealthierappContext();
            DomainManager = new EntityDomainManager<Allergy>(context, Request);
        }

        // GET tables/Allergy
        public IQueryable<Allergy> GetAllAllergy()
        {
            return Query(); 
        }

        // GET tables/Allergy/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<Allergy> GetAllergy(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/Allergy/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<Allergy> PatchAllergy(string id, Delta<Allergy> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/Allergy
        public async Task<IHttpActionResult> PostAllergy(Allergy item)
        {
            Allergy current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/Allergy/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteAllergy(string id)
        {
             return DeleteAsync(id);
        }
    }
}
