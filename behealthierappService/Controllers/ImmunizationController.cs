﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using behealthierappService.DataObjects;
using behealthierappService.Models;

namespace behealthierappService.Controllers
{
    public class ImmunizationController : TableController<Immunization>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            behealthierappContext context = new behealthierappContext();
            DomainManager = new EntityDomainManager<Immunization>(context, Request);
        }

        // GET tables/Immunization
        public IQueryable<Immunization> GetAllImmunization()
        {
            return Query(); 
        }

        // GET tables/Immunization/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<Immunization> GetImmunization(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/Immunization/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<Immunization> PatchImmunization(string id, Delta<Immunization> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/Immunization
        public async Task<IHttpActionResult> PostImmunization(Immunization item)
        {
            Immunization current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/Immunization/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteImmunization(string id)
        {
             return DeleteAsync(id);
        }
    }
}
