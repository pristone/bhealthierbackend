﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using behealthierappService.DataObjects;
using behealthierappService.Models;

namespace behealthierappService.Controllers
{
    public class ProviderProfileController : TableController<ProviderProfile>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            behealthierappContext context = new behealthierappContext();
            DomainManager = new EntityDomainManager<ProviderProfile>(context, Request);
        }

        // GET tables/ProviderProfile
        public IQueryable<ProviderProfile> GetAllProviderProfile()
        {
            return Query(); 
        }

        // GET tables/ProviderProfile/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<ProviderProfile> GetProviderProfile(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/ProviderProfile/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<ProviderProfile> PatchProviderProfile(string id, Delta<ProviderProfile> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/ProviderProfile
        public async Task<IHttpActionResult> PostProviderProfile(ProviderProfile item)
        {
            ProviderProfile current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/ProviderProfile/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteProviderProfile(string id)
        {
             return DeleteAsync(id);
        }
    }
}
