﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using behealthierappService.DataObjects;
using behealthierappService.Models;

namespace behealthierappService.Controllers
{
    public class AlertRecordController : TableController<AlertRecord>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            behealthierappContext context = new behealthierappContext();
            DomainManager = new EntityDomainManager<AlertRecord>(context, Request);
        }

        // GET tables/AlertRecord
        public IQueryable<AlertRecord> GetAllAlertRecord()
        {
            return Query(); 
        }

        // GET tables/AlertRecord/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<AlertRecord> GetAlertRecord(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/AlertRecord/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<AlertRecord> PatchAlertRecord(string id, Delta<AlertRecord> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/AlertRecord
        public async Task<IHttpActionResult> PostAlertRecord(AlertRecord item)
        {
            AlertRecord current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/AlertRecord/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteAlertRecord(string id)
        {
             return DeleteAsync(id);
        }
    }
}
