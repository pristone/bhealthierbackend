﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using behealthierappService.DataObjects;
using behealthierappService.Models;

namespace behealthierappService.Controllers
{
    public class LabResultController : TableController<LabResult>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            behealthierappContext context = new behealthierappContext();
            DomainManager = new EntityDomainManager<LabResult>(context, Request);
        }

        // GET tables/LabResult
        public IQueryable<LabResult> GetAllLabResult()
        {
            return Query(); 
        }

        // GET tables/LabResult/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<LabResult> GetLabResult(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/LabResult/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<LabResult> PatchLabResult(string id, Delta<LabResult> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/LabResult
        public async Task<IHttpActionResult> PostLabResult(LabResult item)
        {
            LabResult current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/LabResult/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteLabResult(string id)
        {
             return DeleteAsync(id);
        }
    }
}
