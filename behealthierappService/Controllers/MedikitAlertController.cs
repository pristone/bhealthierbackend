﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using behealthierappService.DataObjects;
using behealthierappService.Models;

namespace behealthierappService.Controllers
{
    public class MedikitAlertController : TableController<MedikitAlert>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            behealthierappContext context = new behealthierappContext();
            DomainManager = new EntityDomainManager<MedikitAlert>(context, Request);
        }

        // GET tables/MedikitAlert
        public IQueryable<MedikitAlert> GetAllMedikitAlert()
        {
            return Query(); 
        }

        // GET tables/MedikitAlert/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<MedikitAlert> GetMedikitAlert(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/MedikitAlert/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<MedikitAlert> PatchMedikitAlert(string id, Delta<MedikitAlert> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/MedikitAlert
        public async Task<IHttpActionResult> PostMedikitAlert(MedikitAlert item)
        {
            MedikitAlert current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/MedikitAlert/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteMedikitAlert(string id)
        {
             return DeleteAsync(id);
        }
    }
}
