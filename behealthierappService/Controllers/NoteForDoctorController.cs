﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using behealthierappService.DataObjects;
using behealthierappService.Models;

namespace behealthierappService.Controllers
{
    public class NoteForDoctorController : TableController<NoteForDoctor>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            behealthierappContext context = new behealthierappContext();
            DomainManager = new EntityDomainManager<NoteForDoctor>(context, Request);
        }

        // GET tables/NoteForDoctor
        public IQueryable<NoteForDoctor> GetAllNoteForDoctor()
        {
            return Query(); 
        }

        // GET tables/NoteForDoctor/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<NoteForDoctor> GetNoteForDoctor(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/NoteForDoctor/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<NoteForDoctor> PatchNoteForDoctor(string id, Delta<NoteForDoctor> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/NoteForDoctor
        public async Task<IHttpActionResult> PostNoteForDoctor(NoteForDoctor item)
        {
            NoteForDoctor current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/NoteForDoctor/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteNoteForDoctor(string id)
        {
             return DeleteAsync(id);
        }
    }
}
