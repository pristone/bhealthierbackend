﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using behealthierappService.DataObjects;
using behealthierappService.Models;

namespace behealthierappService.Controllers
{
    public class AdherenceController : TableController<Adherence>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            behealthierappContext context = new behealthierappContext();
            DomainManager = new EntityDomainManager<Adherence>(context, Request);
        }

        // GET tables/Adherence
        public IQueryable<Adherence> GetAllAdherence()
        {
            return Query(); 
        }

        // GET tables/Adherence/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<Adherence> GetAdherence(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/Adherence/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<Adherence> PatchAdherence(string id, Delta<Adherence> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/Adherence
        public async Task<IHttpActionResult> PostAdherence(Adherence item)
        {
            Adherence current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/Adherence/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteAdherence(string id)
        {
             return DeleteAsync(id);
        }
    }
}
