﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using behealthierappService.DataObjects;
using behealthierappService.Models;

namespace behealthierappService.Controllers
{
    public class HealthInfoController : TableController<HealthInfo>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            behealthierappContext context = new behealthierappContext();
            DomainManager = new EntityDomainManager<HealthInfo>(context, Request);
        }

        // GET tables/HealthInfo
        public IQueryable<HealthInfo> GetAllHealthInfo()
        {
            return Query(); 
        }

        // GET tables/HealthInfo/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<HealthInfo> GetHealthInfo(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/HealthInfo/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<HealthInfo> PatchHealthInfo(string id, Delta<HealthInfo> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/HealthInfo
        public async Task<IHttpActionResult> PostHealthInfo(HealthInfo item)
        {
            HealthInfo current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/HealthInfo/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteHealthInfo(string id)
        {
             return DeleteAsync(id);
        }
    }
}
