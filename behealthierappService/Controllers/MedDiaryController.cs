﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using behealthierappService.DataObjects;
using behealthierappService.Models;

namespace behealthierappService.Controllers
{
    public class MedDiaryController : TableController<MedDiary>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            behealthierappContext context = new behealthierappContext();
            DomainManager = new EntityDomainManager<MedDiary>(context, Request);
        }

        // GET tables/MedDiary
        public IQueryable<MedDiary> GetAllMedDiary()
        {
            return Query(); 
        }

        // GET tables/MedDiary/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<MedDiary> GetMedDiary(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/MedDiary/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<MedDiary> PatchMedDiary(string id, Delta<MedDiary> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/MedDiary
        public async Task<IHttpActionResult> PostMedDiary(MedDiary item)
        {
            MedDiary current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/MedDiary/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteMedDiary(string id)
        {
             return DeleteAsync(id);
        }
    }
}
