﻿using System.Web.Http;
using Microsoft.Azure.Mobile.Server.Config;
using System.Net.Http;
using System.IdentityModel.Tokens;
using System.Net;
using System.Security.Claims;
using behealthierappService.Models;
using System;
using System.Linq;
using behealthierappService.Models.RequestModels;
using behealthierappService.Utils;
using Microsoft.Azure.Mobile.Server.Login;
using System.Security.Cryptography;
using behealthierappService.DataObjects;

namespace behealthierappService.Controllers
{
    [MobileAppController]
    [RoutePrefix("api/auth")]
    public class AuthController : ApiController
    {

        public HttpResponseMessage Post(AuthenticationRequest credentials)
        {
            try
            {
                string username = credentials.username;
                string password = credentials.password;

                // return error if password is not correct
                if (!this.IsPasswordValid(username, password))
                {
                    return this.Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        message = "Invalid username or password",
                        error=true,
                        Username = username
                    });
                }

                behealthierappContext context = new behealthierappContext();
                var account = context.Accounts.Where(a => a.Username == username).SingleOrDefault();
                var userprofile = context.UserProfiles.Where(a => a.AccountId == account.Id).SingleOrDefault();

                if (!userprofile.Authenticated)
                {
                    return this.Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        error = true,
                        message = "Account has not been authenticated!"
                    });

                }

                JwtSecurityToken token = this.GetAuthenticationTokenForUser(username);

                return this.Request.CreateResponse(HttpStatusCode.OK, new
                {
                    Token = token.RawData,
                    error=false,
                    Account  = account
                });
            }
            catch (Exception e)
            {
                return this.Request.CreateResponse(HttpStatusCode.InternalServerError, new
                {
                    Stacktrace = e.StackTrace,
                    ErrorMessage = e.Message,
                    Credentials = credentials
                });
            }


        }

       /* public HttpResponseMessage ForgotPassword(Account account)
        {
        }*/




        private bool IsPasswordValid(string username, string password)
        {

            behealthierappContext context = new behealthierappContext();
            var account = context.Accounts.Where(a => a.Username == username).SingleOrDefault();
            if (account != null)
            {
                var salt = account.Salt;
                var hashedAndSaltedPassword = CustomLoginProviderUtils.hash(password, salt);

                return hashedAndSaltedPassword.SequenceEqual(account.SaltedAndHashedPassword);

            }
            return false;
        }

        private JwtSecurityToken GetAuthenticationTokenForUser(string username)
        {
            var claims = new Claim[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, username)
            };

            var signingKey = this.GetSigningKey();
            var audience = this.GetSiteUrl(); // audience must match the url of the site
            var issuer = this.GetSiteUrl(); // audience must match the url of the site 
            var secrectKey = this.GennerateSecretKey();





            JwtSecurityToken token = AppServiceLoginHandler.CreateToken(
                claims,
                signingKey,
                audience,
                issuer,
                TimeSpan.FromHours(24)
                );

            return token;
        }
        private string GennerateSecretKey()
        {
            var hmac = new HMACSHA256();
            return Convert.ToBase64String(hmac.Key);
        }

        private string GetSiteUrl()
        {
            var settings = this.Configuration.GetMobileAppSettingsProvider().GetMobileAppSettings();

            if (string.IsNullOrEmpty(settings.HostName))
            {
                return "http://localhost";
            }
            else
            {
                return "https://" + settings.HostName + "/";
            }
        }

        private string GetSigningKey()
        {
            var settings = this.Configuration.GetMobileAppSettingsProvider().GetMobileAppSettings();

            if (string.IsNullOrEmpty(settings.HostName))
            {
                // this key is for debuggint and testing purposes only
                // this key should match the one supplied in Startup.MobileApp.cs
                return "GfYVqdtZUJQfghRiaonAeRQRDjytRi47";
            }
            else
            {
                return Environment.GetEnvironmentVariable("WEBSITE_AUTH_SIGNING_KEY");
            }
        }
    }
}
