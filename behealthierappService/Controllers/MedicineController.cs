﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using behealthierappService.DataObjects;
using behealthierappService.Models;

namespace behealthierappService.Controllers
{
    public class MedicineController : TableController<Medicine>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            behealthierappContext context = new behealthierappContext();
            DomainManager = new EntityDomainManager<Medicine>(context, Request);
        }

        // GET tables/Medicine
        public IQueryable<Medicine> GetAllMedicine()
        {
            return Query(); 
        }

        // GET tables/Medicine/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<Medicine> GetMedicine(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/Medicine/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<Medicine> PatchMedicine(string id, Delta<Medicine> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/Medicine
        public async Task<IHttpActionResult> PostMedicine(Medicine item)
        {
            Medicine current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/Medicine/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteMedicine(string id)
        {
             return DeleteAsync(id);
        }
    }
}
