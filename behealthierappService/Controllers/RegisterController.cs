﻿using System.Web.Http;
using Microsoft.Azure.Mobile.Server.Config;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using behealthierappService.Utils;
using behealthierappService.Models;
using behealthierappService.DataObjects;
using System.Net.Http;
using System.Net;
using Twilio;
using behealthierappService.Models.RequestModels;
using System;
using System.Threading.Tasks;

namespace behealthierappService.Controllers
{
    [MobileAppController]
    [RoutePrefix("api/register")]
    [AllowAnonymous]
    public class RegisterController : ApiController
    {

        // POST api/register
        [HttpPost]
        [Route("newuser")]
        public HttpResponseMessage NewUser(RegistrationRequest registrationRequest)
        {
            try
            {
                if (!Regex.IsMatch(registrationRequest.username, "^[a-zA-Z0-9]{4,}$"))
                {
                    return this.Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        message = "Invalid username (at least 4 chars, alphanumeric only)",
                        error = true
                    });
                }
                else if (registrationRequest.password.Length < 8)
                {
                    return this.Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        message = "Invalid password (at least 8 chars required)",
                        error = true
                    });
                }

                behealthierappContext context = new behealthierappContext();
                DataObjects.Account account = context.Accounts.Where(a => a.Username == registrationRequest.username).SingleOrDefault();
                if (account != null)
                {
                    return this.Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        message = "That username already exists.",
                        error = true
                    });
                }
                else
                {
                    byte[] salt = CustomLoginProviderUtils.generateSalt();
                    DataObjects.Account newAccount = new DataObjects.Account
                    {
                        Id = Guid.NewGuid().ToString(),
                        Username = registrationRequest.username,
                        Salt = salt,
                        CreatedAt = DateTime.Now,
                        SaltedAndHashedPassword = CustomLoginProviderUtils.hash(registrationRequest.password, salt)
                    };
                    context.Accounts.Add(newAccount);
                    context.SaveChanges();

                    //TODO: Externalize the sender to the configs
                    var authenticationCode = GenerateAuthenticationCode(8);

                    UserProfile newUserProfile = new UserProfile
                    {
                        Id = Guid.NewGuid().ToString(),
                        CreatedAt = DateTime.Now,
                        UpdatedAt = DateTime.Now,
                        FirstName = registrationRequest.firstname,
                        LastName = registrationRequest.lastname,
                        Dob = registrationRequest.dob,
                        Ethnicity = registrationRequest.ethnicity,
                        Email = registrationRequest.email,
                        AcceptedTermsOfService = registrationRequest.acceptedtermsofservice,
                        Authenticated = false,
                        Gender = registrationRequest.sex,
                        AuthenticationCode = authenticationCode,
                        AccountId = newAccount.Id,
                        PhoneNum = registrationRequest.phonenum
                    };
                    context.UserProfiles.Add(newUserProfile);
                    context.SaveChanges();

                    //Now send the authentication code to the user
                    this.SendTwilioAuthenticationCode(authenticationCode, registrationRequest.phonenum);

                    return this.Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        message = "Account created successfully!",
                        error = false,
                        userprofile=newUserProfile
                    });
                }
            }
            catch (Exception e)
            {
                return this.Request.CreateResponse(HttpStatusCode.OK, new
                {
                    message = e.Message,
                    error = true
                });
            }
            
            
        }
        [HttpGet]
        [Route("resendcode")]
        public HttpResponseMessage ResendCode(string userProfileId)
        {
            try
            {
                behealthierappContext context = new behealthierappContext();
                var userProfile = context.UserProfiles.Where(a => a.Id == userProfileId).SingleOrDefault();
                if (userProfile == null)
                {
                    return this.Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        message = "Invalid user!",
                        error = true
                    });
                }
                var result = SendTwilioAuthenticationCode(userProfile.AuthenticationCode, userProfile.PhoneNum);
                if (result == null)
                {
                    return this.Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        message = "Could not send out authenticatin code. Please try again",
                        error = true
                    });
                }
                else
                {
                    return this.Request.CreateResponse(HttpStatusCode.OK, new
                    {
                        message = "Authentication code sent!",
                        error = false
                    });
                }
            }
            catch (Exception ex)
            {
                return this.Request.CreateResponse(HttpStatusCode.OK, new
                {
                    message = ex.Message,
                    error = true
                });
            }
        }

        [HttpPost]
        [Route("validatecode")]
        public HttpResponseMessage ValidateCode(AuthorizationCodeRequest authorizationCodeRequest)
        {
            behealthierappContext context = new behealthierappContext();
            var profile=context.UserProfiles.Where(u => u.AccountId == authorizationCodeRequest.userId && u.AuthenticationCode == authorizationCodeRequest.authenticationCode).FirstOrDefault();
            if (profile==null)
            {
                return this.Request.CreateResponse(HttpStatusCode.OK, new
                {
                    message = "Invalid authentication code. Please try again",
                    error = true
                });
            }

            profile.Authenticated = true;
            context.SaveChanges();

            return this.Request.CreateResponse(HttpStatusCode.OK, new
            {
                message = "Code validated.",
                error = false
            });
        }
        private string SendTwilioAuthenticationCode(string authenticationCode, string to)
        {
            try
            {

                // Create an instance of the Twilio client.
                TwilioRestClient client;
                client = new TwilioRestClient(TwilioUtils.ACCOUNTSID, TwilioUtils.AUTHTOKEN);

                // Send an SMS message.
                //TODO: Change the from number
                Message result = client.SendMessage(
                    "+1 469-416-3287", to, authenticationCode);

                if (result.RestException != null)
                {
                    //an exception occurred making the REST call
                    string message = result.RestException.Message;
                    return null;
                }
                return authenticationCode;
            }
            catch (Exception)
            {
                throw;
            }

        }

        private string GenerateAuthenticationCode(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
