﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using behealthierappService.DataObjects;
using behealthierappService.Models;

namespace behealthierappService.Controllers
{
    public class RefillSettingController : TableController<RefillSetting>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            behealthierappContext context = new behealthierappContext();
            DomainManager = new EntityDomainManager<RefillSetting>(context, Request);
        }

        // GET tables/RefillSetting
        public IQueryable<RefillSetting> GetAllRefillSetting()
        {
            return Query(); 
        }

        // GET tables/RefillSetting/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<RefillSetting> GetRefillSetting(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/RefillSetting/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<RefillSetting> PatchRefillSetting(string id, Delta<RefillSetting> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/RefillSetting
        public async Task<IHttpActionResult> PostRefillSetting(RefillSetting item)
        {
            RefillSetting current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/RefillSetting/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteRefillSetting(string id)
        {
             return DeleteAsync(id);
        }
    }
}
