﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using behealthierappService.DataObjects;
using behealthierappService.Models;

namespace behealthierappService.Controllers
{
    public class SupplyController : TableController<Supply>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            behealthierappContext context = new behealthierappContext();
            DomainManager = new EntityDomainManager<Supply>(context, Request);
        }

        // GET tables/Supply
        public IQueryable<Supply> GetAllSupply()
        {
            return Query(); 
        }

        // GET tables/Supply/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<Supply> GetSupply(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/Supply/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<Supply> PatchSupply(string id, Delta<Supply> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/Supply
        public async Task<IHttpActionResult> PostSupply(Supply item)
        {
            Supply current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/Supply/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteSupply(string id)
        {
             return DeleteAsync(id);
        }
    }
}
